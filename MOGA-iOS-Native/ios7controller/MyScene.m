#import <GameController/GameController.h>
#import "MyScene.h"
#import "Player.h"

enum {MAX_CONTROLLERS = 4};
enum {SPRITE_SIZE = 20};

@interface MyScene ()

@property (nonatomic) NSTimeInterval lastUpdate;
@property (strong, nonatomic) NSMutableArray *players;
@property (strong, nonatomic) NSMutableArray *oldSprites;

@end

@implementation MyScene

#pragma mark - SETUP

- (id)initWithSize:(CGSize)size {
    
    if (self = [super initWithSize:size]) {
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
    }
    
    return self;
}

- (void)dealloc
{
    [self removeControllerNotifications];
}

// Get array of players; or
// create an array of MAX_CONTROLLERS * NSNulls
- (NSMutableArray *)players
{
    if (_players == nil)
    {
        _players = [[NSMutableArray alloc] initWithCapacity:MAX_CONTROLLERS];
    }
    
    return _players;
}

// Holds dead, waiting to be cleaned up sprites
- (NSMutableArray *)oldSprites
{
    if (_oldSprites == nil)
    {
        _oldSprites = [[NSMutableArray alloc] init];
    }
    
    return _oldSprites;
}

#pragma mark - NOTIFICATIONS

// Setup receiving of controller connection and disconnection messages
- (void)addControllerNotifications
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self selector:@selector(controllerDidConnect:)
                               name:GCControllerDidConnectNotification object:nil];
    
    [notificationCenter addObserver:self selector:@selector(controllerDidDisconnect:)
                               name:GCControllerDidDisconnectNotification object:nil];
}

// Remove controller connection and disconnection messages
- (void)removeControllerNotifications
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter removeObserver:self name:GCControllerDidConnectNotification object:nil];
    
    [notificationCenter removeObserver:self name:GCControllerDidDisconnectNotification object:nil];
}

#pragma mark - CONTROLLER

- (void)controllerSetup
{
    NSUInteger numOfControllers = [[GCController controllers] count];
    
    // Iterate through the array of controllers;
    // Bind each one's controls a player; and
    
    for (int i = 0; i < MIN(numOfControllers, MAX_CONTROLLERS); i++)
    {
        GCController *controller = [[GCController controllers] objectAtIndex:i];
        
        [self setupNewPlayerController:controller];
    }
    
    // Add the connection / disconection notifications to the
    // notification centre
    [self addControllerNotifications];
}

// On connect create a player for the connected controller
- (void)controllerDidConnect:(NSNotification *)connectedControllerNotif
{
    // Grab the controller object from the notification
    GCController *newController = (GCController *)connectedControllerNotif.object;
    
    [self setupNewPlayerController:newController];
}

// On disconnect remove the controller and cleanup the screen
- (void)controllerDidDisconnect:(NSNotification *)disconnectedControllerNotif
{
    // Grab the controller object from the notification
    GCController *disconnectedController = (GCController *)disconnectedControllerNotif.object;
    
    for (Player *player in self.players)
    {
        if (disconnectedController == player.controller)
        {
            // Remove player from the game
            
            [self.oldSprites addObject:player.sprite];
            
            [self.players removeObject:player];
            
            return;
        }
    }
}

// Create a new player object and bind its movement to the given controller
- (void)setupNewPlayerController:(GCController *)controller
{
    Player *player = [[Player alloc] init];
    
    // Setup the controller's inputs
    [self bindInputsForController:controller toPlayer:player];

    
    [self.players addObject:player];
    
    // Set the controller's playerIndex,
    // this value also lights the appropriate LED on the controller
    controller.playerIndex = [self.players indexOfObject:player];
    
    // For our own reference, associate the controller with a player
    player.controller = controller;
}

// Attach a controller's inputs to an on screen player
- (void)bindInputsForController:(GCController *)controller toPlayer:(Player *)player
{
    // Create a movement handler block which handles thumbstick and dpad movement
    GCControllerDirectionPadValueChangedHandler movementHandler = ^(GCControllerDirectionPad *dpad, float xValue, float yValue)
    {
        // This block sets the player's movement direction
        float length = hypotf(xValue, yValue);
        
        if (length > 0.0f)
        {
            float invLength = 1.0f / length;
            
            player.movementDirection = CGPointMake(xValue * invLength, yValue * invLength);
            
        } else {
            
            player.movementDirection = CGPointZero;
            
        }
    };
    
    // Assign this block to the thumbstick's value changed handler
    controller.extendedGamepad.leftThumbstick.valueChangedHandler = movementHandler;
    
    // Incase the user doesn't have a MOGA or other controller using the full
    // extendedGamepad profile, we also assign the handler to the dpad
    controller.gamepad.dpad.valueChangedHandler = movementHandler;

    // Alter the player's size based on the amount of pressure currently being applied to the A button
    controller.extendedGamepad.buttonA.valueChangedHandler = ^(GCControllerButtonInput *button, float value, BOOL pressed) {
        
        player.playerSize = value;
        
    };
    
    controller.controllerPausedHandler = ^(GCController *controller) {
        
        NSLog(@"Player %li paused game", (long)controller.playerIndex);
        
        // Pause logic here
        
    };
}

#pragma mark - GAME

- (void)update:(CFTimeInterval)currentTime
{
    // Cycle through players
    for (Player *player in self.players)
    {
        // Create new sprites for players
        if (player.sprite == nil)
        {
            player.sprite = [[SKSpriteNode alloc] initWithColor:[UIColor redColor] size:CGSizeMake(SPRITE_SIZE, SPRITE_SIZE)];
            player.sprite.position = CGPointMake(self.view.bounds.size.height/ 2, self.view.bounds.size.width / 2);
            [self addChild:player.sprite];
        }
        
        // Move everybody
        if (hypotf(player.movementDirection.x, player.movementDirection.y) > 0.0f)
        {
            CGFloat dx = player.movementDirection.x;
            CGFloat dy = player.movementDirection.y;
            
            player.sprite.position = CGPointMake(player.sprite.position.x + dx, player.sprite.position.y + dy);
        }
        
        // Alter character's size
        CGFloat newDimension = SPRITE_SIZE + (player.playerSize * SPRITE_SIZE * 2);
        
        if (newDimension != player.sprite.size.width)
        {
            player.sprite.size = CGSizeMake(newDimension, newDimension);
        }
    }
    
    // Remove a disconnected player's old sprites from the world
    [self removeChildrenInArray:self.oldSprites];
    self.oldSprites = nil;
    
    self.lastUpdate = currentTime;
}

@end