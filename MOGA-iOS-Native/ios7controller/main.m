//
//  main.m
//  ios7controller
//
//  Created by Stephen Wojcik on 19/02/2014.
//  Copyright (c) 2014 bda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
