//
//  Player.h
//  ios7controller
//
//  Created by Stephen Wojcik on 20/02/2014.
//  Copyright (c) 2014 bda. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GCController, SKSpriteNode;

@interface Player : NSObject

@property (strong, nonatomic) GCController *controller;
@property (strong, nonatomic) SKSpriteNode *sprite;
@property (nonatomic) CGPoint movementDirection;
@property (nonatomic) CGFloat playerSize;

@end
